
/* 
 * C++ properties
 * By Vokhmin Ivan <cabalbl4@gmail.com> - https://github.com/Cabalbl4/cpp-properties
 * Released under the MIT license
 */
 
#ifndef PROPERTY_H
#define PROPERTY_H
#include <stdexcept>
#include <typeinfo>
#include <list>
#include <map>
#include <string>

/**
 * @brief basic property for enumeration purposes
 * @abstract
 */
class base_property {
public:
    
    base_property() : m_readOnly(false) {}
    
    base_property(bool readOnly): m_readOnly(readOnly) {}

    virtual ~base_property(){}

    /**
    * @brief readOnly
    * @return
    */
   bool readOnly() {
       return m_readOnly;
   }

   /**
    * @brief setReadOnly
    * @param state to set
    */
   void setReadOnly(bool state) {
     m_readOnly = state;
   }

   /**
    * @brief type
    * @return value type string RTTI (compiler dependable)
    */
   virtual std::string type() = 0;

   /**
    * @brief type std::type_info.hash_code()
    * @return
    */
   virtual size_t typeHash() = 0;

   /**
    * @brief compare value type info with other value type info
    * @param other other value
    * @return true if types are same
    */
   virtual bool cmpType(const std::type_info & other) = 0;

   /**
    * @brief get value as void pointer variable
    * @return void*
    */
   virtual void* getVariableAsVoidPtr() =0;

protected:
   bool m_readOnly;
};





/**
 * @brief property class
 * @details
 * Usage: property<type, className> name;
 * @tparam S type
 * @tparam CLASSNAME class to generate property for
 */

template <typename S, class CLASSNAME> class property : public base_property {
public:


   property(S value, CLASSNAME * classPtr,
            S(CLASSNAME::*getter)(property<S, CLASSNAME>&),
            void(CLASSNAME::*setter)(property<S, CLASSNAME>&,S),
            bool readOnly=false): base_property(readOnly),
        m_getter(getter),
        m_setter(setter),
        m_constSetter(nullptr),
        m_value(value),
        m_class(classPtr)
    {}
   property(S value, CLASSNAME * classPtr,
            S(CLASSNAME::*getter)(property<S, CLASSNAME>&),
            void(CLASSNAME::*setter)(property<S, CLASSNAME>&, const S&),
            bool readOnly=false): base_property(readOnly),
        m_getter(getter),
        m_setter(nullptr),
        m_constSetter(setter),
        m_value(value),
        m_class(classPtr)
    {}

  property(S value,CLASSNAME * classPtr,
           S(CLASSNAME::*getter)(property<S, CLASSNAME>&),
           bool readOnly=false) : base_property(readOnly),
      m_getter(getter),
      m_setter(nullptr),
      m_constSetter(nullptr),
      m_value(value),
      m_class(classPtr)

  {}

  property(S value,CLASSNAME * classPtr,
           void(CLASSNAME::*setter)(property<S, CLASSNAME>&,S),
           bool readOnly=false) : base_property(readOnly),
      m_getter(nullptr),
      m_setter(setter),
      m_constSetter(nullptr),
      m_value(value),
      m_class(classPtr)
  {}
  property(S value,CLASSNAME * classPtr,
           void(CLASSNAME::*setter)(property<S, CLASSNAME>&,const S&),
           bool readOnly=false) : base_property(readOnly),
      m_getter(nullptr),
      m_setter(nullptr),
      m_constSetter(setter),
      m_value(value),
      m_class(classPtr)
  {}

  property(S value,CLASSNAME * classPtr,
           bool readOnly=false) : base_property(readOnly),
      m_getter(nullptr),
      m_setter(nullptr),
      m_constSetter(nullptr),
      m_value(value),
      m_class(classPtr)
   {}

  property() : base_property(false),
      m_getter(nullptr),
      m_setter(nullptr),
      m_constSetter(nullptr),
      m_class(nullptr)
   {}

  virtual ~property(){}

  virtual std::string type() override {
    return typeid(m_value).name();
  }

  virtual size_t typeHash() override {
      return typeid(m_value).hash_code();
  }

  virtual bool cmpType(const std::type_info &other) {
    return (typeid(m_value) == other);
  }



  /**
   * @brief get value, calls getter if defined
   * @return value
   */
  S get() {
    if(m_getter == nullptr) {
      return m_value;
    } else {
      return (m_class->*m_getter)(*this);
    }

  }

  void* getVariableAsVoidPtr() override {
    return reinterpret_cast<void*>(valuePtr());
  }

  /**
   * @brief set value, calls setter if defined
   * @throws std::runtime_error if read-only
   * @param val new value
   */
  void set(S val) throw (std::runtime_error) {
      if(m_readOnly) throw std::runtime_error("Read only property modification attempt");
      if((m_setter == nullptr) && (m_constSetter== nullptr)) {
              m_value = val;
      } else {
             if(m_setter != nullptr) {
                 (m_class->*m_setter)(*this,val);
             } else {
                 (m_class->*m_constSetter)(*this,val);
             }
      }
  }

  /**
   * @brief get value pointer
   * @return pointer to value for modification purposes in getters and setters
   */
  S* valuePtr() { return &m_value; }

  /**
   * @brief setGetter
   * @param classPtr pointer to class to set property for
   * @param getter function - class member ptr. Accepts 1 params - 1 property link
   */
  void setGetter(CLASSNAME * classPtr, S(CLASSNAME::*getter)(property<S, CLASSNAME>&)) {
      m_getter = getter;
      m_class = classPtr;
  }

  /**
   * @brief setSetter
   * @param classPtr pointer to class to set property for
   * @param setter function - class member ptr.  Accepts 2 params - 1 property link, 2 new value
   */
  void setSetter(CLASSNAME * classPtr, void(CLASSNAME::*setter)(property<S, CLASSNAME>&,S)) {
      m_setter = setter;
      m_constSetter = nullptr;
      m_class = classPtr;
  }

  /**
   * @brief setSetter (const ref)
   * @param classPtr pointer to class to set property for
   * @param setter function - class member ptr.  Accepts 2 params - 1 property link, 2 new value by const ref
   */
  void setSetter(CLASSNAME * classPtr, void(CLASSNAME::*setter)(property<S, CLASSNAME>&,const S&)) {
      m_setter = nullptr;
      m_constSetter = setter;
      m_class = classPtr;
  }



property<S,CLASSNAME>& operator=(S rhs){
      set(rhs);
      return *this;
  }

/**
  Property compare
 * @brief operator ==
 * @param rhs
 * @return true if values are ==
 */
inline bool operator==(const property& rhs){
    return this.m_value == rhs.m_value;
}

/**
   Property compare ne
 * @brief operator !=
 * @param rhs
 * @return ! == bool
 */
inline bool operator!=(const property& rhs){
    return !(this == rhs);
}

/**
   Value compare
 * @brief operator ==
 * @param rhs
 * @return
 */
inline bool operator==(const S& rhs) {
    return (m_value == rhs);
}
/**
  Value ne compare
 * @brief operator !=
 * @param rhs
 * @return
 */
inline bool operator!=(const S& rhs) {
    return !(this == rhs);
}

template <typename U, typename T>
    friend std::ostream& operator<<( std::ostream&, property<U, T>& );

template <typename U, typename T>
     friend std::istream& operator>>(std::istream&, property<U,T>& );

protected:
     S(CLASSNAME::*m_getter)(property<S, CLASSNAME>&);
     void(CLASSNAME::*m_setter)(property<S, CLASSNAME>&, const S);
     void(CLASSNAME::*m_constSetter)(property<S, CLASSNAME>&, const S&);
     S m_value;
     CLASSNAME *m_class;


};


template<typename T,typename U>
std::ostream& operator<<(std::ostream& out, property<T,U>& what) {
    out<<what.get();
    return out;
}

template<typename T,typename U>
std::istream& operator>>(std::istream& is, property<T,U>& what) {
    T var;
    var << is;
    what.set(var);
    return is;
}




#define properties public:\
    std::map<std::string, base_property*> _properties;\
    private:\

#define expose_property(name) _properties[#name] = &(this->name);


#endif // PROPERTY_H
