#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>

#include "property.h"

class Test {
    properties
public:
    property<std::string, Test> st;
    property<int, Test> st1;
    property<bool, Test> st2;
    property<bool, Test> st3;
    property<std::string, Test> st4;
    property<std::string, Test> st5;
//Accessors can be private
private:
    void testSetter(property<std::string, Test> &param, std::string A) {
        std::cout<<"USE SETTER"<<std::endl;
        *(param.valuePtr()) = A;
    }
    
    void testSetter2(property<std::string, Test> &param, const std::string & A) {
        std::cout<<"USE SETTER"<<std::endl;
        *(param.valuePtr()) = A;
    }


    std::string testGetter(property<std::string, Test> &param) {
        std::cout<<"USE GETTER"<<std::endl;
        return *(param.valuePtr());
    }
public:
    // Init in constructor
    Test() : st5(property<std::string, Test>("st5Test", this, &Test::testGetter, &Test::testSetter, true)) {
        expose_property(st1);
        expose_property(st);
        expose_property(st2);
        // Set getter
        st.setGetter(this, &Test::testGetter);
        // Set setter
        st.setSetter(this, &Test::testSetter);
        st4.setSetter(this, &Test::testSetter2);
        st = "test override";
        st1 =2;
        st3.setReadOnly(true);
        //Later init
        st4 = property<std::string, Test>("st4Test", this, &Test::testGetter, &Test::testSetter, true);

    }



};


int main()
{
  Test t;
  // print property value, trigger getters
  std::cout<<t.st<<t.st1<<std::endl;
  t.st1 = 1;
  std::cout<<t.st<<std::endl;
  // trigger setters
  t.st = "test1";
  t.st = "test2";
  t.st = "test3";
  //t.st3 is read-only, must throw error
  try {
    t.st3 = false;
  } catch(std::runtime_error e) {
      std::cout<<e.what()<<std::endl;
  }
  // get value, trigger getter
  t.st4.get();
  std::cout<<t.st4.type()<<std::endl;
  
  //Enumerate properties
  for(std::map<std::string,base_property*>::iterator it = t._properties.begin(); it != t._properties.end(); ++it) {
    std::cout << "exposed property " << it->first << std::endl;;
  }
  
    //Get the type of basic property as string (RTTI returns "i" for int)
  std::cout<<"type of ptoperty st1:"<<t._properties["st1"]->type()<<std::endl;

  //Compare by typeid
  std::cout<<"cmp by typeid:"<<t._properties["st1"]->cmpType(typeid(int))<<std::endl;

  //dynamic cast basic property
  property<int, Test>* castedProperty = dynamic_cast<property<int,Test>*>(t._properties["st1"]);
  // be sure to check castedProperty != nullptr
  std::cout<<"get casted st1:"<<castedProperty->get()<<std::endl;

  std::cout<<"st5:"<<t.st5<<std::endl;

  std::string testCompare = "st5Test";
  //By value comparation via overloaded ==
  std::cout<<(t.st5==testCompare)<<std::endl;
  std::cout<<(t.st5.get()==testCompare)<<std::endl;

  // Compare after get
  std::cout<<(t.st1.get()>0)<<std::endl;

}
