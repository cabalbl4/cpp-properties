# cpp-properties
Simple way of adding properties to C++ classes without dependencies

This was written in attempt to mimic JavaScript/Delphi object property behavior. Someone may find this helpful;

## License
MIT

## How to use
Just include property.h in your project

## Requirements
C++ 2011 + RTTI

## Usage example 

```C++
class Test {
// This macros adds public map of properties, named "_properties". Use it with expose_property macro
    properties
public:
    property<std::string, Test> st;
    property<int, Test> st1;
    property<bool, Test> st2;
    property<bool, Test> st3;
    property<std::string, Test> st4;


    void testSetter(property<std::string, Test> &param, std::string A) {
        std::cout<<"USE SETTER"<<std::endl;
        *(param.valuePtr()) = A;
    }

    std::string testGetter(property<std::string, Test> &param) {
        std::cout<<"USE GETTER"<<std::endl;
        return *(param.valuePtr());
    }
    
    // Another setter signature
    void testSetter2(property<std::string, Test> &param, const std::string & A) {
        std::cout<<"USE SETTER"<<std::endl;
        *(param.valuePtr()) = A;
    }

    Test() {
        // This function in constructor simply adds properties to "_properties" map. properties macro must be present for this to work
        expose_property(st1);
        expose_property(st);
        expose_property(st2);
	// Add getters/setters after construction
        st.setGetter(this, &Test::testGetter);
        st.setSetter(this, &Test::testSetter);
        st = "test override";
        st1 =2;
        st3.setReadOnly(true);
        //Init getters/setters via constructor
        st4 = property<std::string, Test>("st4Test", this, &Test::testGetter, &Test::testSetter, true);

    }
 }

```

Be advised, that "properties" and "expose_property" macro are optional


More of usage example - see main.cpp 


